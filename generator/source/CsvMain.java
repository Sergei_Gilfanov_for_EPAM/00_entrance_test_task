import java.util.*;
import java.io.IOException;
import java.io.Reader;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import org.apache.commons.cli.*;

import java.nio.charset.IllegalCharsetNameException;

public class CsvMain
{
	public void boot(String[] args) throws IOException
	{
		String colStr;
		int col;
		Option colOpt = Option.builder("col")
			.argName("число")
			.required(true)
			.hasArg()
			.desc("Количество колонок в генерируемом файле")
			.build();

		String rowStr;
		int row;
		Option rowOpt = Option.builder("row")
			.argName("число")
			.required(true)
			.hasArg()
			.desc("Количество строк в генерируемом файле")
			.build();

		String lenStr;
		int len;
		Option lenOpt = Option.builder("len")
			.argName("число")
			.required(true)
			.hasArg()
			.desc("Максимальная длина строковых полей")
			.build();

		String outputFileName;
		
		Option outOpt = Option.builder("out")
			.argName("файл")
			.required(true)
			.hasArg()
			.desc("Выходной файл")
			.build();

		String charsetName;
		Option encOpt = Option.builder("enc")
			.argName("строка")
			.required(true)
			.hasArg()
			.desc("Кодировка выходного файла")
			.build();

		boolean noeol = false;
		Option noeolOpt = Option.builder("noeol")
			.required(false)
			.desc("(не обязательна) Не генерировать переводы строк в данных")
			.build();

		Options options = new Options();
		options.addOption(colOpt);
		options.addOption(rowOpt);
		options.addOption(lenOpt);
		options.addOption(outOpt);
		options.addOption(encOpt);
		options.addOption(noeolOpt);

		CommandLineParser parser = new DefaultParser();


		try {
			CommandLine line = parser.parse(options, args);
			charsetName = line.getOptionValue("enc");
			outputFileName = line.getOptionValue("out");
			lenStr = line.getOptionValue("len");
			colStr = line.getOptionValue("col");
			rowStr = line.getOptionValue("row");
			if (line.hasOption("noeol")) {
				noeol = true;
			}

		} catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			// Шаманство. formater.printHelp, если позволить ему
			// писать в System.out самому, полностью игнорирует кодировку
			// Поэтому сначала мы перенаправляем его вывод в строку,
			// а потому уже эту строку печатаем.
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			formatter.printHelp( pw, 80, "CsvMain", "", options, 4, 2, "" );
			pw.flush();
			System.out.print(sw.toString());
			return;
		}

		// Парсим числовые опции

		try {
			col = Integer.parseInt(colStr);
		} catch (NumberFormatException e) {
			System.err.println("Значение параметра col (" + colStr + ") не распознается как число");
			return;
		}

		try {
			row = Integer.parseInt(rowStr);
		} catch (NumberFormatException e) {
			System.err.println("Значение параметра row (" + rowStr + ") не распознается как число");
			return;
		}

		try {
			len = Integer.parseInt(lenStr);
		} catch (NumberFormatException e) {
			System.err.println("Значение параметра len (" + lenStr + ") не распознается как число");
			return;
		}

		boolean badCharset = false;
		CharsetInfo ci = null;
		try {
			ci = new CharsetInfo(charsetName);
		} catch ( IllegalArgumentException e) {
			System.err.println("Кодировка '" + charsetName + "' не распознается");
			return;
		}

		RandomCsv r = new RandomCsv(ci, len, col, row, noeol);

		try {
			FileOutputStream outputStream  = new FileOutputStream(outputFileName);
			OutputStreamWriter unbufferedWriter = new OutputStreamWriter(outputStream, ci.charset);
			BufferedWriter bufferedWriter = new BufferedWriter(unbufferedWriter);

			RandomCsv csv = new RandomCsv(ci, len, col, row, noeol);
			csv.writeTo(bufferedWriter);

			bufferedWriter.close();
			unbufferedWriter.close();
			outputStream.close();
		} catch (IOException e) {
			System.err.println(e.toString());
			return;
		}
	}

	public static void main(String[] args) throws IOException
	{
		new CsvMain().boot(args);
	}
}
