import java.io.IOException;
import java.io.Writer;

public class FieldInteger extends Field {

	public String titleSuffix()
	{
		return " Integer";
	}

	public void writeDataTo(Writer w) throws IOException
	{
		String s = generator.nextIntegerAsString();
		w.write(s);
	}
}

