import java.util.regex.*;
class FieldInteger extends Field
{
	static private Pattern pattern = Pattern.compile("^-?[1-9]\\d*$");
	public FieldInteger(String aName)
	{
		super(aName);
	}

	public boolean verifyFormat(String s)
	{
		return pattern.matcher(s).matches();
	}
	public boolean search(String field, String needle)
	{
		return field.equals(needle);
	}
	public String suffix()
	{
		return " Integer";
	}
}
