import java.util.*;
import java.io.IOException;
import java.io.Reader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import org.apache.commons.cli.*;

import java.nio.charset.IllegalCharsetNameException;

public class CsvMain
{
	public void boot(String[] args) throws IOException
	{

		String inFileName;
		Option inFileNameOpt = Option.builder("in")
			.argName("строка")
			.required(true)
			.hasArg()
			.desc("Имя входного файла")
			.build();


		String outFileName;
		Option outFileNameOpt = Option.builder("out")
			.argName("строка")
			.required(true)
			.hasArg()
			.desc("Имя выходного файла")
			.build();

		String charsetName;
		Option charsetNameOpt = Option.builder("enc")
			.argName("строка")
			.required(true)
			.hasArg()
			.desc("Кодировка выходного файла")
			.build();

		String colStr;
		Option colOpt = Option.builder("col")
			.argName("строка")
			.required(true)
			.hasArg()
			.desc("Имя столбца для поиска")
			.build();

		String expStr;
		Option expOpt = Option.builder("exp")
			.argName("строка")
			.required(true)
			.hasArg()
			.desc("Строка для поиска")
			.build();

		Options options = new Options();
		options.addOption(inFileNameOpt);
		options.addOption(outFileNameOpt);
		options.addOption(charsetNameOpt);
		options.addOption(colOpt);
		options.addOption(expOpt);

		CommandLineParser parser = new DefaultParser();


		try {
			CommandLine line = parser.parse(options, args);
			inFileName = line.getOptionValue("in");
			outFileName = line.getOptionValue("out");
			charsetName = line.getOptionValue("enc");
			colStr = line.getOptionValue("col");
			expStr = line.getOptionValue("exp");

		} catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			// Шаманство. formater.printHelp, если позволить ему
			// писать в System.out самому, полностью игнорирует кодировку
			// Поэтому сначала мы перенаправляем его вывод в строку,
			// а потому уже эту строку печатаем.
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			formatter.printHelp( pw, 80, "CsvMain", "", options, 4, 2, "" );
			pw.flush();
			System.out.print(sw.toString());
			return;
		}

		Charset charset = null;
		try {
			charset = Charset.forName(charsetName);
		} catch ( IllegalArgumentException e) {
			System.err.println("Кодировка '" + charsetName + "' не распознается");
			return;
		}


		try {
			InputStream inputStream = new FileInputStream(inFileName);
			InputStreamReader unbufferedReader = new InputStreamReader(inputStream, charset);
			BufferedReader bufferedReader = new BufferedReader(unbufferedReader);
			
			
			Csv csv = new Csv(bufferedReader);

			FileOutputStream outputStream  = new FileOutputStream(outFileName);
			OutputStreamWriter unbufferedWriter = new OutputStreamWriter(outputStream, charset);
			BufferedWriter bufferedWriter = new BufferedWriter(unbufferedWriter);

			csv.search(bufferedWriter, colStr, expStr);

			bufferedWriter.close();
			unbufferedWriter.close();
			outputStream.close();
		} catch (IOException e) {
			System.err.println(e.toString());
			return;
		} 
	}

	public static void main(String[] args) throws IOException
	{
		new CsvMain().boot(args);
	}
}
