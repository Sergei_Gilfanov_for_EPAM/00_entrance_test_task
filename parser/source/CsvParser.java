import java.util.Deque;
import java.util.ArrayDeque;
import java.io.IOException;
import java.io.Reader;
import java.io.PushbackReader;
import java.lang.StringBuilder;

class CsvParser {
	private PushbackReader reader;
	// Тут будем накапливать уже обработанные символы из входного потока,
	// пополняем каждый раз, когда будем делать read из reader.
	private StringBuilder parsed;
	// Но только когда не делали unread;
	private boolean thereWasUnread;
	private Deque<Integer> outputBuffer;

	public CsvParser(Reader r) {
		reader = new PushbackReader(r,1);
		outputBuffer = new ArrayDeque<Integer>();
		parsed = new StringBuilder();
		thereWasUnread = false;
	}

	public static final int END_OF_FIELD = -1;
	public static final int END_OF_RECORD = -2;
	public static final int END_OF_INPUT = -3;

	/* Возвращает очередной символ из потока чтения, снимая кавычки
	 * при этом:
	 *
	 * Заковыченные "" возвращаются как "
	 *
	 * Незаковыченная ; возвращается как число END_OF_FIELD 
	 * (окончание поля)
	 *
	 * Незаковыченные \n, \r, \r\n, возвращаются как последовательность
	 * чисел END_OF_FIELD, END_OF_RECORD (окончание поля, окончание записи)
	 *
	 * Окончание потока возвращается как 
	 * END_OF_FIELD, END_OF_RECORD, END_OF_INPUT
	 * (окончание поля, окончание записи, окончание ввода)
	 */
	public int unquoteRead() throws IOException
	{
		if (outputBuffer.size() == 0) {
			unquoteParseNext();
		}
		// Конец ввода остается в буфере навсегда
		if (outputBuffer.peek() == -3) {
			return END_OF_INPUT;
		}
		int retval = outputBuffer.remove();
		return retval;
	}

	/* Возвращает очередной символ из потока чтения. Не понимает
	 * смысла кавычек.
	 *
	 * ; возвращается как число END_OF_FIELD (окончание поля)
	 *
	 * \n, \r, \r\n, возвращаются как последовательность
	 * чисел END_OF_FIELD, END_OF_RECORD (окончание поля, окончание записи)
	 *
	 * Окончание потока возвращается как
	 * END_OF_FIELD, END_OF_RECORD, END_OF_INPUT
	 * (окончание поля, окончание записи, окончание ввода)
	 */
	public int rawRead() throws IOException
	{

		if (outputBuffer.size() == 0) {
			rawParseNext();
		}
		// Конец ввода остается в буфере навсегда
		if (outputBuffer.peek() == -3) {
			return END_OF_INPUT;
		}
		int retval = outputBuffer.remove();
		return retval;
	}

	public int peek() throws IOException
	{
		if (outputBuffer.size() == 0) {
			unquoteParseNext();
		}
		return outputBuffer.getFirst();
	}

	public String parsed() {
		return parsed.toString();
	}

	public void cleanParsed() {
		parsed = new StringBuilder();
	}

	private int read() throws IOException
	{
		int c = reader.read();
		if ( thereWasUnread ) {
			thereWasUnread = false;
		} else {
			if (c >= 0) {
				parsed.append((char)c);
			}
		}
		return c;
	}
	// PushbackReader не любит, когда ему пытаются вернуть обратно конец файла
	// поэтому будем использовать обертку
	private void unread(int c) throws IOException
	{
		if (c>=0) {
			reader.unread(c);
			thereWasUnread = true;
		}
	}

	private enum ParserState {
		S_START,
		S_Q,    // "
		S_CR,    // \r (CR)
		S_QS,	// ";  (Quote, Semicolon)
		S_QCR,	// "\r (Quote, CR)
		S_QLF,   // "\n (Quote, LF)
		S_QCRLF,  // "\r\n (Quote, CR, LF)
		S_END
	}

	// Чтение и снятие кавычек с входного потока.
	private void unquoteParseNext() throws IOException
	{
		int retval[] = null;
		int c = 0;

		ParserState state = ParserState.S_START;

		while (state != ParserState.S_END) {
			c = read();
/*			System.out.println("++++");
			System.out.println(c);
			System.out.println(state);*/
			switch(state) 
			{
				case S_START:
					switch(c) 
					{
						case '"':
							state = ParserState.S_Q;
							break;
						case '\r':
							state = ParserState.S_CR;
							break;
						case ';':
							retval = new int[]{-1};
							state = ParserState.S_END;
							break;
						case '\n':
							retval = new int[]{END_OF_FIELD, END_OF_RECORD};
							state = ParserState.S_END;
							break;
						case -1:
							retval = new int[]{END_OF_FIELD, END_OF_RECORD, END_OF_INPUT};
							state = ParserState.S_END;
							break;
						default:
							retval = new int[]{c};
							state = ParserState.S_END;
					};
					break;
				case S_Q: // "
					switch(c)
					{
						case ';': // ";
							state = ParserState.S_QS;
							break;
						case '\r': // "\r
							state = ParserState.S_QCR;
							break;
						case '\n': // "\n
							state = ParserState.S_QLF;
							break;
						case '"': // ""
							retval = new int[]{'"'};
							state = ParserState.S_END;
							break;
						default: // "<что-то еще
							// TODO ругаться на неправильную кавычку
							unread(c);
							state = ParserState.S_START;
					};
					break;
				case S_CR: // \r
					switch(c)
					{
						case '\n': // \r\n
							retval = new int[]{-1, -2};
							state = ParserState.S_END;
							break;
						default: // \r<что-то еще
							retval = new int[]{-1, -2};
							unread(c);
							state = ParserState.S_START;
					};
					break;

				case S_QS: // ";
					switch (c)
					{
						case '"': // ";"
							retval = new int[]{';'};
							state = ParserState.S_END;
							break;
						default: // ";<что-то еще>
							// TODO ругаться на неправильную кавычку
							retval = new int[] {';'};
							unread(c);
							state = ParserState.S_END;
					};
					break;
				case S_QCR: // "\r
					switch (c)
					{
						case '\n': // "\r\n
							state = ParserState.S_QCRLF;
							break;
						case '"': // "\r"
							retval = new int[]{'\r'};
							state = ParserState.S_END;
							break;
						default: // "\r<что-то еще>
							// TODO ругаться на неправильную кавычку
							retval = new int[] {'\r'};
							unread(c);
							state = ParserState.S_END;
					};
					break;
				case S_QLF: // "\n
					switch (c)
					{
						case '"': // "\n"
							retval = new int[]{'\n'};
							state = ParserState.S_END;
							break;
						default: // "\n<что-то еще>
							// TODO ругаться на неправильную кавычку
							retval = new int[] {'\n'};
							unread(c);
							state = ParserState.S_END;
					};
					break;
				case S_QCRLF: // "\r\n
					switch (c)
					{
						case '"': // "\r\n"
							retval = new int[]{'\r','\n'};
							state = ParserState.S_END;
							break;
						default: // "\r\n<что-то еще>
							// TODO ругаться на неправильную кавычку
							retval = new int[] {END_OF_FIELD, END_OF_RECORD};
							unread(c);
							state = ParserState.S_END;
					};
					break;
			}
/*			System.out.println(c);
			System.out.println(state);
			System.out.println("----");*/
		}
		for(int ci: retval) {
			outputBuffer.add(ci);
		}
	}

	private void rawParseNext() throws IOException
	{
		int retval[] = null;
		int c = 0;

		ParserState state = ParserState.S_START;

		while (state != ParserState.S_END) {
			c = read();
/*			System.out.println("++++");
			System.out.println(c);
			System.out.println(state);*/
			switch(state) 
			{
				case S_START:
					switch(c) 
					{
						case '\r':
							state = ParserState.S_CR;
							break;
						case ';':
							retval = new int[]{-1};
							state = ParserState.S_END;
							break;
						case '\n':
							retval = new int[]{END_OF_FIELD, END_OF_RECORD};
							state = ParserState.S_END;
							break;
						case -1:
							retval = new int[]{END_OF_FIELD, END_OF_RECORD, END_OF_INPUT};
							state = ParserState.S_END;
							break;
						default:
							retval = new int[]{c};
							state = ParserState.S_END;
					};
					break;
				case S_CR: // \r
					switch(c)
					{
						case '\n': // \r\n
							retval = new int[]{-1, -2};
							state = ParserState.S_END;
							break;
						default: // \r<что-то еще
							retval = new int[]{-1, -2};
							unread(c);
							state = ParserState.S_START;
					};
					break;
			}
/*			System.out.println(c);
			System.out.println(state);
			System.out.println("----");*/
		}
		for(int ci: retval) {
			outputBuffer.add(ci);
		}
	}
}
