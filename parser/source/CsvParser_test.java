import java.util.*;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.io.IOException;


import java.io.IOException;

public class CsvParser_test
{
	public static void main(String[] args) throws IOException
	{
	/*	String charsetName = "utf8";
		Charset charset;
		try {
			charset = Charset.forName(charsetName);
		} catch ( IllegalArgumentException e) {
			System.err.println("Кодировка  не распознается");
			return;
		}

		try {
			InputStream inputStream = new FileInputStream("newline_demo.csv");
			InputStreamReader reader = new InputStreamReader(inputStream, charset);
		} catch (IOException ioe) {
			//System.err.println(ioe);
		    //System.err.println("oops " + ioe.getMessage());
			ioe.printStackTrace();
		}
	*/
/*
		// Разные положения разделителя колонок
		parseString ("012345");
		parseString (";012345");
		parseString ("012;345");
		parseString ("012345;");

		// Разделитель колонок в кавычке
		parseString ("012\";\"345");
		// Неправильные кавычки
		parseString ("012\";345");

		// Разные положения и разные разделители строк
		parseString ("\n012345abcd");
		parseString ("012345\nabcd");
		parseString ("012345abcd\n");
		parseString ("\r012345abcdabcd");
		parseString ("012345\rabcd");
		parseString ("012345abcd\r");

		// Разделители строк в кавычках
		parseString ("\"\n\"012345abcd");
		parseString ("012345\"\n\"abcd");
		parseString ("012345abcd\"\n\"");
		parseString ("\"\r\"012345abcdabcd");
		parseString ("012345\"\r\"abcd");
		parseString ("012345abcd\"\r\"");

		// Незаконченная кавычка для разделителя
		parseString ("\"\n012345abcd");
		parseString ("012345\"\nabcd");
		parseString ("012345abcd\"\n");
		parseString ("\"\r012345abcdabcd");
		parseString ("012345\"\rabcd");
		parseString ("012345abcd\"\r");

		// DOS Разделители строк
		parseString ("\r\n012345abcd");
		parseString ("012345\r\nabcd");
		parseString ("012345abcd\r\n");
		// Незаконченная кавычка для DOS разделителя
		parseString ("\"\r\n012345abcd");
		parseString ("012345\"\r\nabcd");
		parseString ("012345abcd\"\r\n");

		// Просто неправильная кавычка
		parseString ("01\"2\"345abcd");
		parseString ("01\"2\"\"345abcd");
		parseString ("012345abcd\"");*/

		// 012345abcd"\n"";"
		parseString ("012345abcd\"\n\"\";\"");
		parseString ("012345abcd\"\r\n\"\";\"");


		rawParseString (";ab\"cd");
		rawParseString ("ab;c\"de");
		rawParseString ("abc\"d;e");
		rawParseString ("ab\";cde");
		rawParseString ("ab;\"cde");

	}
	

	public static void parseString(String s) throws IOException
	{
		Reader r = new StringReader(s);
		CsvParser ur = new CsvParser(r);
		List<Integer> l = new ArrayList<Integer>();
		int c;
		do {
			c = ur.unquoteRead();
			l.add(c);
		} while (c != -3);

		
		System.out.print("[" + s + "] -> [ ");
		for (int c2: l) {
			if (c2>0) {
				System.out.print((char)c2);
			} else {
				System.out.print(c2);
			}
			System.out.print(" ");
		}
		System.out.println("]");
	}


	public static void rawParseString(String s) throws IOException
	{
		Reader r = new StringReader(s);
		CsvParser ur = new CsvParser(r);
		List<Integer> l = new ArrayList<Integer>();
		int c;
		do {
			c = ur.rawRead();
			l.add(c);
		} while (c != -3);

		
		System.out.print("[" + s + "] -> [ ");
		for (int c2: l) {
			if (c2>0) {
				System.out.print((char)c2);
			} else {
				System.out.print(c2);
			}
			System.out.print(" ");
		}
		System.out.println("]");
	}
}
