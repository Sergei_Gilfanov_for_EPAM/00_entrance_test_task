import java.util.regex.*;
class FieldFloat extends Field
{
	// десятичное число с запятой в качестве разделителя
	static private Pattern pattern = Pattern.compile("^-?(([1-9]\\d+)|(\\d)),\\d+$");
	public FieldFloat(String aName)
	{
		super(aName);
	}

	// TODO/FIXME пока сравнение посимвольное.
	// В результате 123,10 не будет ровняться 123,1
	public boolean verifyFormat(String s)
	{
		return pattern.matcher(s).matches();
	}
	public boolean search(String field, String needle)
	{
		return field.equals(needle);
	}
	public String suffix()
	{
		return " Float";
	}
}
