abstract class Field
{
	private String name;

	public Field(String aName)
	{
		name = aName;
	}

	abstract public boolean verifyFormat(String s);
	abstract public boolean search(String field, String needle);
	abstract public String suffix();

	public String getName()
	{
		return  name;
	}
}
